import Vue from 'vue';
import Router from 'vue-router';
import Login from '@/components/Login';
import HelloWorld from '@/components/HelloWorld';
import Dashboard from '@/components/Dashboard';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/', // 첫 화면을 로그인 화면으로 설정한다
      name: 'Login',
      component: Login,
    },
    {
      path: '/helloWorld',
      name: 'HelloWorld',
      component: HelloWorld,
      //beforeEnter: requireAuth()
    },
    {
      path: '/dashboard',
      name: 'Dashboard',
      component: Dashboard,
      //beforeEnter: requireAuth()
    },
  ],
});