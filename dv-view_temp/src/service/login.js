import axios from 'axios';
import store from '../vuex/store';

export default {
  login(uid, password) {
    var api1 = this.getUserInfo(uid, password);
    Promise
      .all([ api1 ])
      .then(function() {
        return store.state.isAuth;
      });
  },
  getUserInfo(uid, password) {
    return axios.post('http://localhost:10000/login', { uid: uid, password: password })
      .then(function(response) {
        if (response.data.id == 'myborn') {
          store.commit('IS_AUTH', true);
          store.commit('UID', response.data.id);
        }
      });
  },
};
