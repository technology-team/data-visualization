import Vue from 'vue';
import App from './App.vue';
import store from './vuex/store'; // vuex 저장소 추가
import router from './routers';
import Vuetify from 'vuetify';
import 'bootstrap/dist/css/bootstrap.css';
// include all css files
import './lib/VuelyCss';

//plugin

Vue.use(Vuetify, {
  theme: {
    primary: '#5D92F4',
    secondary: '#424242',
    accent: '#82B1FF',
    error: '#FF3739',
    info: '#00D0BD',
    success: '#00D014',
    warning: '#FFB70F',
  },
});

Vue.config.productionTip = false;

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App),
});