import * as types from './mutation_types';

export default {
  [ types.UID ](state, uid) {
    state.uid = uid;
  },
  [ types.UNAME ](state, uname) {
    state.uname = uname;
  },
  [ types.IS_AUTH ](state, isAuth) {
    state.isAuth = isAuth;
  },
};