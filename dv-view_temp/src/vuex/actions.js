import { IS_AUTH } from './mutation_types';


export default {
  setIsAuth({ commit }, data) {
    commit(IS_AUTH, data);
  },
};