import Vue from 'vue';
import Vuex from 'vuex';
import getters from './getters';
import actions from './actions';
import mutations from './mutations';
import primaryTheme from './themes/primaryTheme';

Vue.use(Vuex);

const state = {
  uid: '',
  uname: '',
  isAuth: false,
  theme: primaryTheme,
};

export default new Vuex.Store({
  state,
  mutations,
  getters,
  actions,
});