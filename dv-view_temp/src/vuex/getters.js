export default {
  getUid: state => state.uid,
  getUname: state => state.uname,
  getIsAuth: state => state.isAuth,
  theme: state => state.theme,

};