package net.suby.auth.login.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.suby.auth.login.domain.Member;
import net.suby.auth.login.domain.MemberRoleId;

@Repository
public interface MemberRoleRepository extends JpaRepository<Member, MemberRoleId> {
}
