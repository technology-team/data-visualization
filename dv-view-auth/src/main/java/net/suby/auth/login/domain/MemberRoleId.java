package net.suby.auth.login.domain;

import java.io.Serializable;

public class MemberRoleId implements Serializable {
    private String id;
    private String userRole;
}
