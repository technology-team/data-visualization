package net.suby.auth.login.domain;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import org.springframework.context.annotation.Lazy;

import lombok.Getter;
import lombok.Setter;

@Entity(name = "member")
@Setter
@Getter
public class Member {
    @Id
    private String id;
    private String password;
    private String name;

    @OneToMany
    @Lazy
    @JoinColumn(name = "id")
    private List<MemberRole> MemberRole;
}
