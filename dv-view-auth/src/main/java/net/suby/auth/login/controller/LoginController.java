package net.suby.auth.login.controller;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import net.suby.auth.login.dto.LoginDto;
import net.suby.auth.login.service.LoginService;

@RestController
public class LoginController {
    private LoginService loginService;

    public LoginController(LoginService loginService) {
        this.loginService = loginService;
    }

    @PostMapping("/login")
    public void login(@RequestBody @Valid LoginDto loginDto) {
        System.out.println("aaa");
        this.loginService.login(loginDto);
    }
}
