package net.suby.auth.login.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.suby.auth.login.domain.Member;

@Repository
public interface MemberRepository extends JpaRepository<Member, String> {
}
