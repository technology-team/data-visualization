package net.suby.dv.login.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import net.suby.dv.login.dto.LoginDto;

@RestController
public class LoginController {

    @PostMapping("/login")
    public Map<String, String> login(@RequestBody LoginDto loginDto) {
        Map<String, String> result = new HashMap<>();
        if ("myborn".equals(loginDto.getUid())) {
            result.put("id", "myborn");
            result.put("name", "나의탄생");
        } else {
            result.put("error", "no exists user");
        }
        return result;
    }

    @GetMapping("/login")
    public Map<String, String> getLogin(LoginDto loginDto) {
        Map<String, String> result = new HashMap<>();
        if ("myborn".equals(loginDto.getUid())) {
            result.put("id", "myborn");
            result.put("name", "나의탄생");
        } else {
            result.put("error", "no exists user");
        }
        return result;
    }
}
