package net.suby.dv.login.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class LoginDto {
    private String uid;
    private String name;
}
