import en from './en';
import fr from './fr';
import he from './he';
import ru from './ru';
import ar from './ar';
import kr from './kr';

export default {
  kr: {
    message: kr,
  },
  en: {
    message: en,
  },
  fr: {
    message: fr,
  },
  he: {
    message: he,
  },
  ru: {
    message: ru,
  },
  ar: {
    message: ar,
  },

};