package net.suby.oauth.login.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.suby.oauth.login.persistence.Member;

@Repository
public interface MemberRepository extends JpaRepository<Member, String> {
}
