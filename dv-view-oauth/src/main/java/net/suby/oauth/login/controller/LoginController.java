package net.suby.oauth.login.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import net.suby.oauth.login.service.LoginService;

@RestController
public class LoginController {
    private LoginService loginService;

    public LoginController(LoginService loginService) {
        this.loginService = loginService;
    }

    @PostMapping("/success")
    public void login() {
        System.out.println("aaaa");
    }

    @GetMapping("/success")
    public void loginSuccess() {
        System.out.println("success");
    }

    @GetMapping("/fail")
    public void loginfail() {
        System.out.println("fail");
    }
}
