package net.suby.oauth.login.dto;

import javax.validation.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;


@Setter
@Getter
public class LoginDto {
    @NotBlank
    private String username;
    @NotBlank
    private String password;
}
