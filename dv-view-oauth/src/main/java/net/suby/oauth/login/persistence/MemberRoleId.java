package net.suby.oauth.login.persistence;

import java.io.Serializable;

public class MemberRoleId implements Serializable {
    private String id;
    private String userRole;
}
