package net.suby.oauth.login.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.suby.oauth.login.persistence.Member;
import net.suby.oauth.login.persistence.MemberRoleId;

@Repository
public interface MemberRoleRepository extends JpaRepository<Member, MemberRoleId> {
}
